<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Joheka Classic Tips</title>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark blackBG sticky-top">
            <div class="container">
                <img src="assets/images/IMG-20210803-WA0139.jpg" alt="joheka" id="hImage1" class="nav-brand">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#thisOne"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="thisOne">
                    <ul class="navbar-nav ms-auto">
                        <li class="navbar-item">
                            <a href="#" class="nav-link active">Home</a>
                        </li>
                        <li class="navbar-item">
                            <a href="#" class="nav-link">About</a>
                        </li>
                        <li class="navbar-item">
                            <a href="#" class="nav-link">Contact</a>
                        </li>
                        <li class="navbar-item">
                            <a href="#" class="nav-link">Register</a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>

    </header>

    <main>
        <section class="container">
            <div class="row row-cols-1 row-cols-sm-1 row-cols-md-2 row-cols-lg-2">
                <!-- info -->
                <div class="col infom d-none d-md-block">
                    <h2 class="text-center">Joheka Classic tips International</h2>
                    <p>We offer daily match betting previews and analysis for every major or minor league around the world.<br>
                        Whether you want predictions for today, tommorrow or any day of the week, we have you covered
                    </p>
                    <p>Join our VIP and VVIP telegram channels to get more tips</p>
                    <button class="btn btn-lg bg-dark btn-outline-danger w-100" type="button">VIP Groups</button>
                    <br><a href="#Plans" class="btn btn-lg btn-danger text-center w-100" hidden>VIP Groups</a>
                </div>
                <!-- images -->
                <div class="col">
                    <div class="imgSliders  mt-1">
                        <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
                            <div class="carousel-inner imgDiv">
                                <div class="carousel-item active" data-bs-interval="4000">
                                    <img src="assets/images/IMG-20210803-WA0139.jpg" class="d-block w-100 slideImage" alt="img 1">
                                </div>
                                <div class="carousel-item" data-bs-interval="4000">
                                    <img src="assets/images/IMG-20220619-WA0024.jpg" class="d-block w-100 slideImage" alt="img 12">
                                </div>
                                <div class="carousel-item" data-bs-interval="4000">
                                    <img src="assets/images/IMG-20220619-WA0025.jpg" class="d-block w-100 slideImage" alt="img 21">
                                </div>
                                <div class="carousel-item" data-bs-interval="4000">
                                    <img src="assets/images/IMG-20220619-WA0030.jpg" class="d-block w-100 slideImage" alt="img 2">
                                </div>
                                <div class="carousel-item">
                                    <img src="assets/images/IMG-20220619-WA0026.jpg" class="d-block w-100 slideImage" alt="img 3">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--Today Games-->
        <section class="container">
            <h3 class="shadow-lg rounded hTitle mt-2">Today Games predictions</h3>
            <!-- Games -->
            <section class="LLoogs">
                <section class="obj1 mt-0 shadow-lg rounded bg-light">
                    <div class="tnl">
                        <p>5 : 45 am</p>
                        <p></p>
                        <!-- <p class="rDot mt-2" id="d1"></p> -->
                        <p style="color: red">live</p>
                    </div>
                    <div class="tns">
                        <div class="teams">
                            <p>Manchester</p>
                            <p class="d-none d-sm-block">v/s</p>
                            <p>Chelsea</p>
                        </div>
                        <div class="scores">
                            <p>?</p>
                            <p>:</p>
                            <p>?</p>
                        </div>
                    </div>
                    <div class="predict">
                        <p>Tip: Home over 1.5</p>
                    </div>
                </section>

                <section class="obj1 mt-0 shadow-lg rounded">
                    <div class="tnl">
                        <p>5 : 45 am</p>
                        <p></p>
                        <!-- <p class="rDot mt-2" id="d1"></p> -->
                        <p style="color: black">Played</p>
                    </div>
                    <div class="tns">
                        <div class="teams">
                            <p>Manchester</p>
                            <p class="d-none d-sm-block">v/s</p>
                            <p>Chelsea</p>
                        </div>
                        <div class="scores">
                            <p>?</p>
                            <p>:</p>
                            <p>?</p>
                        </div>
                    </div>
                    <div class="predict">
                        <p>Tip: Home over 1.5</p>
                    </div>
                </section>

                <section class="obj1 mt-0 shadow-lg rounded">
                    <div class="tnl">
                        <p>5 : 45 am</p>
                        <p></p>
                        <!-- <p class="rDot mt-2" id="d1"></p> -->
                        <p style="color: blue">Pending</p>
                    </div>
                    <div class="tns">
                        <div class="teams">
                            <p>Manchester</p>
                            <p class="d-none d-sm-block">v/s</p>
                            <p>Chelsea</p>
                        </div>
                        <div class="scores">
                            <p>?</p>
                            <p>:</p>
                            <p>?</p>
                        </div>
                    </div>
                    <div class="predict">
                        <p>Tip: Home over 1.5</p>
                    </div>
                </section>

                <section class="obj1 mt-0 shadow-lg rounded">
                    <div class="tnl">
                        <p>5 : 45 am</p>
                        <p></p>
                        <p class="rDot mt-2" id="d1"></p>
                    </div>
                    <div class="tns">
                        <div class="teams">
                            <p>Manchester</p>
                            <p class="d-none d-sm-block">v/s</p>
                            <p>Chelsea</p>
                        </div>
                        <div class="scores">
                            <p>?</p>
                            <p>:</p>
                            <p>?</p>
                        </div>
                    </div>
                    <div class="predict">
                        <p>Tip: Home over 1.5</p>
                    </div>
                </section>
            </section>
        </section>

        <!-- Telegram link -->
        <section class="container mb-1 mt-0 mt-lg-2">
            <div class="balk blackBG">
                <marquee behavior="" direction="left"><b>Join our telegram channel <i class="fa fa-hand-o-down"> <i class="fa fa-hand-o-down"> <i class="fa fa-hand-o-down"></i></i></b></marquee>
                <h5 class="text-center"><a href="https://t.me/johekaclassictips2020channel"><i class="fa fa-telegram">Telegram</i></a></h5>
            </div>
        </section>

        <!--Previous games-->
        <section class="container">
            <h3 class="shadow-lg rounded hTitle mt-1">Yesterday predictions results</h3>
            <!-- Games Section -->
            <section class="LLoogs">
                <section class="obj1 los mt-0 shadow rounded">
                    <div class="tns">
                        <div class="teams">
                            <p>Manchester</p>
                            <p class="d-none d-sm-block">v/s</p>
                            <p>Arsenal</p>
                        </div>

                        <div class="scores">
                            <p>5</p>
                            <p>:</p>
                            <p>1</p>
                        </div>
                    </div>
                    <div class="predict">
                        <p>Prediction: GG</p>
                    </div>
                </section>
                <section class="obj1 won mt-0 shadow rounded">
                    <div class="tns">
                        <div class="teams">
                            <p>Manchester</p>
                            <p class="d-none d-sm-block">v/s</p>
                            <p>Bayern Muchen</p>
                        </div>
                        <div class="scores">
                            <p>2</p>
                            <p>:</p>
                            <p>1</p>
                        </div>
                    </div>
                    <div class="predict">
                        <p>Prediction: Home over 1.5</p>
                    </div>
                </section>
            </section>
            <!-- Statistics -->
            <div class="statistics text-center">
                <h1>Statistics</h1>
                <p>Tips given: 6</p>
                <p>Accurate tips: 5</p>
                <p>Lose: 1</p>
                <p>Prediction accuracy: 83%</p>
            </div>
        </section>

        <section class="container">
            <h3 class="hTitle bg-danger" id="Plans">VIP Packages Plan</h3>
            <section class="Plan1">
                <div class="plans">
                    <h5><u>Gold VIP Plan - 2+ Odds</u></h5>
                    <p>We shall be providing strictly 2.0+ odds in this channel, with a higher win rate to double our clients stake.<br>
                        Kshs 3000/=(30$)</p>
                </div>
                <div class="plans">
                    <h5><u>Platinum Plan - 10 to 25+ Odds</u></h5>
                    <p>We shall be provinding with 10+ to 25+ odds single ticket everyday.<br> WIN Rate: 99%</br>
                        Kshs 3,500/=(35$)</p>
                </div>
                <div class="plans">
                    <h5><u>Diamond Plan - 1.30+ Bankers</u></h5>
                    <p style="font-size: small;">We shall be providing with 1.30+ single ticket daily<br> Target 3M in 21 days<br> WIN rate: 100%</br>
                        Kshs 2,200/=(20$)</p>
                </div>
                <div class="plans">
                    <h5><u>Silver Plan - 1.50+ Odds</u></h5>
                    <p style="font-size: small;">We shall be providing 1.50+ odds single ticket daily for 15 day<br> Our main target to hit is 1M </br>
                        Kshs 1800/=(18$)</p>
                </div>
            </section>
        </section>
        <!-- here -->
        
    </main>


    <footer class="lblackBG">
        <div class="container text-light blackBG">
            <section class="row row-cols row-cols-sm-1 row-cols-md-1 row-cols-lg-3">
                <div class="col">
                    <h4>Joheka Classic Tips.</h4>
                    <p style="font-size: medium;">Joheka classic tips International, free predictions, previews for main and minor football leagues update everyday.
                        Check out all our winning <em>1X2</em> betting tips for today<br> Joheka classic tips is always up-to-date, it provides mathematical
                        football predictions generated by computer algorithm on the basisof statistics.
                    </p>
                </div>
                <div class="col text-center ">
                    <h4>Contact Us.</h4>
                    <section id="socialLinks">
                        <div class="d-inline p-2"><a href="https://www.facebook.com/Johekaclassictips2020/" target="_blank"><i class="fa fa-facebook"></i></a></div>
                        <div class="d-inline p-2"><a href="https://www.instagram.com/invites/contact/?i=v84au7094bp3&utm_content=isrnfx5" target="_blank"><i class="fa fa-instagram"></i></a></div>
                        <div class="d-inline p-2"><a href="https://t.me/johekaclassictips2020channel" target="_blank"><i class="fa fa-paper-plane"></i></a></div>
                        <div class="d-inline p-2"><a href="https://twitter.com/JohekaT?t=v18hfCZ1uUE0Wm6oEJ-SWg&s=09" target="_blank"><i class="fa fa-twitter"></i></a></div>
                        <div class="d-inline p-2"><a href="mailto: johekaclassictips2020@gmail.com"><i class="fa fa-envelope-open"></i></a></div>
                    </section>
                    <ul class="mt-3">
                        <li><a href="tel: +254791805170"><i class="fa fa-phone"> +254791805170</i></a></li>
                        <li><a href="https://api.whatsapp.com/send?phone=+254759306980"><i class="fa fa-whatsapp"> +254759306980</i></a></li>
                    </ul>
                </div>
                <div class="col text-center ">
                    <h4>Other Links.</h4>
                    <ul>
                        <li><a href="#">Melbet</a></li>
                        <li><a href="#">Livescore</a></li>
                    </ul>
                </div>
            </section>

            <div class="cRights">
                <p><i>© 2022 Copyright.</i><br>
                    Terms and Conditions apply.
                </p>
            </div>
        </div>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>