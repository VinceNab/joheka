<?php include_once('fixed/header.php')?>

        <div class="main">
            <!--Image Slider-->
            <div class="row row-cols-1 row-cols-sm-1 row-cols-md-2 row-cols-lg-2">
                <div class="col infom d-none d-md-block">
                    <center>
                        <h2>Joheka Classic tips International</h2>
                    </center>
                    <p>We offer daily match betting previews and analysis for every major or minor league around the world.<br>
                        Whether you want predictions for today, tommorrow or any day of the week, we have you covered
                    </p>

                    <p>Join our VIP and VVIP telegram channels to get more tips</p>
                    <center>
                        <a href="#Plans" class="btn btn-danger">VIP Groups</a>
                    </center>
                </div>
                <div class="col">
                    <div class="imgSliders  mt-1">
                        <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active" data-bs-interval="4000">
                                    <img src="assets/images/IMG-20210803-WA0139.jpg" class="d-block w-100 slideImage" alt="img 1">
                                </div>
                                <div class="carousel-item" data-bs-interval="4000">
                                    <img src="assets/images/IMG-20220619-WA0024.jpg" class="d-block w-100 slideImage" alt="img 12">
                                </div>
                                <div class="carousel-item" data-bs-interval="4000">
                                    <img src="assets/images/IMG-20220619-WA0025.jpg" class="d-block w-100 slideImage" alt="img 21">
                                </div>
                                <div class="carousel-item" data-bs-interval="4000">
                                    <img src="assets/images/IMG-20220619-WA0030.jpg" class="d-block w-100 slideImage" alt="img 2">
                                </div>
                                <div class="carousel-item">
                                    <img src="assets/images/IMG-20220619-WA0026.jpg" class="d-block w-100 slideImage" alt="img 3">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--Today Games-->
            <section>
                <section id="Fball" class="mt-2">
                    <h3 class="hTitle bg-danger">Today Games predictions</h3>
                </section>

                <!-- <div class="indicator mt-1 bg-primary mb-1">
                    <div class="dInline">
                        <section id="d1" class="rDot mt-1"></section>
                        <p>Pending</p>
                    </div>
                    <div class="dInline">
                        <section id="d2" class="rDot mt-1"></section>
                        <p>Live</p>
                    </div>
                    <div class="dInline">
                        <section id="d3" class="rDot mt-1"></section>
                        <p>Ended</p>
                    </div>
                </div> -->

                <section class="LLoogs">
                    <section class="obj1 mt-0">
                        <div class="tnl">
                            <p>5 : 45 am</p>
                            <p></p>
                            <!-- <p class="rDot mt-2" id="d1"></p> -->
                            <p style="color: red">live</p>
                        </div>
                        <div class="tns">
                            <div class="teams">
                                <p>Manchester</p>
                                <p class="d-none d-sm-block">v/s</p>
                                <p>Chelsea</p>
                            </div>
                            <div class="scores">
                                <p>?</p>
                                <p>:</p>
                                <p>?</p>
                            </div>
                        </div>
                        <div class="predict">
                            <p>Tip: Home over 1.5</p>
                        </div>
                    </section>

                    <section class="obj1 mt-0">
                        <div class="tnl">
                            <p>5 : 45 am</p>
                            <p></p>
                            <!-- <p class="rDot mt-2" id="d1"></p> -->
                            <p style="color: black">Played</p>
                        </div>
                        <div class="tns">
                            <div class="teams">
                                <p>Manchester</p>
                                <p class="d-none d-sm-block">v/s</p>
                                <p>Chelsea</p>
                            </div>
                            <div class="scores">
                                <p>?</p>
                                <p>:</p>
                                <p>?</p>
                            </div>
                        </div>
                        <div class="predict">
                            <p>Tip: Home over 1.5</p>
                        </div>
                    </section>

                    <section class="obj1 mt-0">
                        <div class="tnl">
                            <p>5 : 45 am</p>
                            <p></p>
                            <!-- <p class="rDot mt-2" id="d1"></p> -->
                            <p style="color: blue">Pending</p>
                        </div>
                        <div class="tns">
                            <div class="teams">
                                <p>Manchester</p>
                                <p class="d-none d-sm-block">v/s</p>
                                <p>Chelsea</p>
                            </div>
                            <div class="scores">
                                <p>?</p>
                                <p>:</p>
                                <p>?</p>
                            </div>
                        </div>
                        <div class="predict">
                            <p>Tip: Home over 1.5</p>
                        </div>
                    </section>

                    <section class="obj1 mt-0">
                        <div class="tnl">
                            <p>5 : 45 am</p>
                            <p></p>
                            <p class="rDot mt-2" id="d1"></p>
                        </div>
                        <div class="tns">
                            <div class="teams">
                                <p>Manchester</p>
                                <p class="d-none d-sm-block">v/s</p>
                                <p>Chelsea</p>
                            </div>
                            <div class="scores">
                                <p>?</p>
                                <p>:</p>
                                <p>?</p>
                            </div>
                        </div>
                        <div class="predict">
                            <p>Tip: Home over 1.5</p>
                        </div>
                    </section>
                </section>
            </section>

            <!--Previous games-->
            <section>
                <h3 class="hTitle bg-danger mt-1">Yesterday predictions results</h3>
                <section class="LLoogs">
                    <section class="obj1 los mt-0">
                        <div class="tns">
                            <div class="teams">
                                <p>Manchester</p>
                                <p class="d-none d-sm-block">v/s</p>
                                <p>Arsenal</p>
                            </div>

                            <div class="scores">
                                <p>5</p>
                                <p>:</p>
                                <p>1</p>
                            </div>
                        </div>
                        <div class="predict">
                            <p>Prediction: GG</p>
                        </div>
                    </section>
                    <section class="obj1 won mt-0">
                        <div class="tns">
                            <div class="teams">
                                <p>Manchester</p>
                                <p class="d-none d-sm-block">v/s</p>
                                <p>Bayern Muchen</p>
                            </div>
                            <div class="scores">
                                <p>2</p>
                                <p>:</p>
                                <p>1</p>
                            </div>
                        </div>
                        <div class="predict">
                            <p>Prediction: Home over 1.5</p>
                        </div>
                    </section>
                </section>

                <div class="statistics">
                    <center>
                        <h1>Statistics</h1>
                        <p>Tips given: 6</p>
                        <p>Accurate tips: 5</p>
                        <p>Lose: 1</p>
                        <p>Prediction accuracy: 83%</p>
                    </center>
                </div>
            </section>

            <!--Description-->
            <!-- <h3 class="hTitle" id="Plans">VIP Packages Plan</h3>
            <section class="row row-cols-1 row-cols-sm-1 row-cols-md-2 row-cols-lg-4">
                <div class="col plans">
                    <h5><u>Gold VIP Plan - 2+ Odds</u></h5>
                    <p>We shall be providing strictly 2.0+ odds in this channel, with a higher win rate to double our clients stake.<br>
                    Kshs 3000/=(30$)</p>
                </div>
                <div class="col plans">
                    <h5><u>Platinum Plan - 10 to 25+ Odds</u></h5>
                    <p>We shall be provinding with 10+ to 25+ odds single ticket everyday.<br> WIN Rate: 99%</br>
                    Kshs 3,500/=(35$)</p>
                </div>
                <div class="col plans">
                    <h5><u>Diamond Plan - 1.30+ Bankers</u></h5>
                    <p style="font-size: small;">We shall be providing with 1.30+ single ticket daily<br> Target 3M in 21 days<br> WIN rate: 100%</br>
                    Kshs 2,200/=(20$)</p>
                </div>
                <div class="col plans">
                    <h5><u>Silver Plan - 1.50+ Odds</u></h5>
                    <p style="font-size: small;">We shall be providing 1.50+ odds single ticket daily for 15 day<br> Our main target to hit is 1M </br>
                    Kshs 1800/=(18$)</p>
                </div>
            </section> -->

            <!--Description-->
            <h3 class="hTitle bg-danger" id="Plans">VIP Packages Plan</h3>
            <section class="Plan1">
                <div class="plans">
                    <h5><u>Gold VIP Plan - 2+ Odds</u></h5>
                    <p>We shall be providing strictly 2.0+ odds in this channel, with a higher win rate to double our clients stake.<br>
                    Kshs 3000/=(30$)</p>
                </div>
                <div class="plans">
                    <h5><u>Platinum Plan - 10 to 25+ Odds</u></h5>
                    <p>We shall be provinding with 10+ to 25+ odds single ticket everyday.<br> WIN Rate: 99%</br>
                    Kshs 3,500/=(35$)</p>
                </div>
                <div class="plans">
                    <h5><u>Diamond Plan - 1.30+ Bankers</u></h5>
                    <p style="font-size: small;">We shall be providing with 1.30+ single ticket daily<br> Target 3M in 21 days<br> WIN rate: 100%</br>
                    Kshs 2,200/=(20$)</p>
                </div>
                <div class="plans">
                    <h5><u>Silver Plan - 1.50+ Odds</u></h5>
                    <p style="font-size: small;">We shall be providing 1.50+ odds single ticket daily for 15 day<br> Our main target to hit is 1M </br>
                    Kshs 1800/=(18$)</p>
                </div>
            </section>
        </div>

<?php include_once('fixed/footer.php') ?>